set dotenv-load

clean:
	find . -not -name '*.md' -not -name 'PKGBUILD' -not -wholename './.git*' -not -name 'Justfile' -type f | xargs -n1 rm -v

deploy:
	rsync --rsync-path="sudo rsync" -avzr */*.zst "$SSH_LOGIN@$SSH_HOST:$SSH_PATH"
	ssh $SSH_LOGIN@$SSH_HOST "sudo ${SCRIPT}"
